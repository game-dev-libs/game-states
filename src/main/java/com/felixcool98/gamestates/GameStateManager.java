package com.felixcool98.gamestates;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.ApplicationListener;

public class GameStateManager implements ApplicationListener {
	private Set<GameState> states = new HashSet<GameState>();
	
	private GameState next;
	private GameState current;
	
	
	public GameStateManager() {
		
	}
	public GameStateManager(ApplicationListener state) {
		enter(state);
	}
	public GameStateManager(GameState state) {
		enter(state);
	}
	
	
	public void enter(ApplicationListener state) {
		enter(new GameState() {
			@Override
			public void render() {
				state.render();
			}
			
			@Override
			public void create() {
				state.create();
			}
			@Override
			public void dispose() {
				state.dispose();
			}
			
			@Override
			public void enter(GameState previous, GameStateManager manager) {
				state.resume();
			}
			
			@Override
			public void leave() {
				state.pause();
			}
			
			@Override
			public void pause() {
				state.pause();
			}
			@Override
			public void resume() {
				state.resume();
			}
			
			@Override
			public void resize(int width, int height) {
				state.resize(width, height);
			}
		});
	}
	public void enter(GameState state) {
		next = state;
	}
	
	private void nextState() {
		if(next == null)
			return;
		
		if(current != null) {
			current.leave();
		}
		
		GameState previous = current;
		
		current = next;
		next = null;
		
		if(!states.contains(current)) {
			current.create();
			states.add(current);
		}
		
		current.enter(previous, this);
	}
	
	@Override
	public void create() {}
	@Override
	public void pause() {
		if(current != null)
			current.pause();
	}
	@Override
	public void resume() {
		if(current != null)
			current.resume();
		
	}
	@Override
	public void render() {
		nextState();
		
		if(current != null)
			current.render();
	}
	@Override
	public void resize(int width, int height) {
		if(current != null)
			current.resize(width, height);
	}
	
	@Override
	public void dispose() {
		for(GameState state : states)
			state.dispose();
	}
}
