package com.felixcool98.gamestates;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class AdvancedGameState extends GameStateAdapter {
	private Stage stage;
	private boolean autoActDrawStage = true;
	
	private Color clearColor = Color.WHITE;
	private boolean autoClearScreen = true;
	
	private InputMultiplexer multiplexer = new InputMultiplexer();
	
	private GameStateManager manager;
	
	
	public void setStage(Stage stage) {
		setStage(stage, true);
	}
	public void setStage(Stage stage, boolean addToMultiplexer) {
		this.stage = stage;
		
		if(addToMultiplexer)
			multiplexer.addProcessor(stage);
	}
	public void setClearColor(Color clearColor) {
		this.clearColor = clearColor;
	}
	
	
	public Stage getStage() {
		return stage;
	}
	public Color getClearColor() {
		return clearColor;
	}
	public InputMultiplexer getMultiplexer() {
		return multiplexer;
	}
	
	
	/**
	 * sets up input processor and stage<br>
	 * calls {@link ImprovedGameState#created()} afterwards<br>
	 * instead of overwriting this using created is advised
	 */
	@Deprecated
	@Override
	public final void create() {
		setStage(new Stage());
		
		created();
	}
	public void created() {
		
	}
	
	@Override
	public final void enter(GameState previous, GameStateManager manager) {
		Gdx.input.setInputProcessor(getMultiplexer());
		
		this.manager = manager;
	}
	public void entered() {
		
	}
	
	
	public void enter(GameState state) {
		manager.enter(state);
	}
	public void enter(ApplicationListener state) {
		manager.enter(state);
	}
	
	
	//=====================================
	// rendering
	//=====================================
	
	@Deprecated
	@Override
	public void render() {
		if(autoClearScreen)
			clearScreen();
		
		renderGame();
		if(autoActDrawStage)
			renderAndActStage();
	}
	public void clearScreen() {
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}
	public void renderAndActStage() {
		if(stage == null)
			return;
		
		stage.act();
		stage.draw();
	}
	public void renderGame() {
		
	}
	
	
	public void disableAutoScreenClear() {
		autoClearScreen = false;
	}
	public void disableAutoStageActAndRender() {
		autoActDrawStage = false;
	}
	
	
	@Override
	public void resize(int width, int height) {
		if(stage == null)
			return;
		
		getStage().getViewport().setScreenSize(width, height);
	}
}
