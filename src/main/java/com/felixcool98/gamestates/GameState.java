package com.felixcool98.gamestates;

import com.badlogic.gdx.ApplicationListener;

public interface GameState extends ApplicationListener {
	public void render();
	
	public void enter(GameState previous, GameStateManager manager);
	public void leave();
	
	public void resume();
	public void pause();
	
	public void create();
	public void dispose();
	
	public void resize(int width, int height);
}
